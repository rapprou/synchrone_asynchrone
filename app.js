// Function asynchrone callback
// Créer une fonction avec un retour de résultat

const taskOne = function() { 
    console.log("Message function synchrone ");

    }
// C’est une funktion avec un rappel de callback de 3 seconds 
 const taskTwo = function() { 
     console.log("Ce message function asynchrone,.. s'affiche après 3 secondes");
  
 }

// Ensuite l'ntegrar avec d’autres éléments 

 taskOne();
 taskTwo();
//  setTimeout(taskOne, 3000)
setTimeout(taskTwo, 3000)
